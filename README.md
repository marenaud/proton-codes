# README #

Source code for some GEANT user-codes I made. The dose calculator code reads in an egsphant file and a .mac file describing a proton beam (using GPS macros) and outputs a 3ddose file. The track generator file is for track generation for pre-calculated Monte Carlo.