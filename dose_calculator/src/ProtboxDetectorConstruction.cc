#include "ProtboxDetectorConstruction.hh"
#include "ProtboxDetectorMessenger.hh"
#include "G4SDManager.hh"
#include "G4NistManager.hh"
#include "G4RunManager.hh"
#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "globals.hh"
#include "G4UserLimits.hh"
#include "G4PhantomParameterisation.hh"

#include "G4PVParameterised.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4PSDoseDeposit3D.hh"
#include "G4PSDoseDeposit.hh"
#include "MyDoseDeposit.hh"

#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"


#include <vector>

ProtboxDetectorConstruction::ProtboxDetectorConstruction()
 : G4VUserDetectorConstruction() {
  fMessenger = new ProtboxDetectorMessenger(this);
}

ProtboxDetectorConstruction::~ProtboxDetectorConstruction() {
	//delete stepLimit;
  delete fMessenger;
}

G4VPhysicalVolume* ProtboxDetectorConstruction::Construct() {
	G4NistManager* man = G4NistManager::Instance();
  G4int nel;

	G4Material* H2O  = man->FindOrBuildMaterial("G4_WATER");
	G4Material* air  = man->FindOrBuildMaterial("G4_AIR");

  G4Material* bone = man->FindOrBuildMaterial("G4_BONE_CORTICAL_ICRP");

  G4Element* elH  = man->FindOrBuildElement( 1);
  G4Element* elC  = man->FindOrBuildElement( 6);
  G4Element* elN  = man->FindOrBuildElement( 7);
  G4Element* elO  = man->FindOrBuildElement( 8);
  G4Element* elNa = man->FindOrBuildElement(11);
  G4Element* elP  = man->FindOrBuildElement(15);
  G4Element* elS  = man->FindOrBuildElement(16);
  G4Element* elCl = man->FindOrBuildElement(17);
  G4Element* elK  = man->FindOrBuildElement(19);

  G4Material *lung = new G4Material("LUNG",0.26 * g/cm3,nel=9);
  lung->AddElement(elH, 0.103);
  lung->AddElement(elC, 0.105);
  lung->AddElement(elN, 0.031);
  lung->AddElement(elO, 0.749);
  lung->AddElement(elNa,0.002);
  lung->AddElement(elP, 0.002);
  lung->AddElement(elS, 0.003);
  lung->AddElement(elCl,0.003);
  lung->AddElement(elK, 0.002);

	G4double protbox_x = 500.0*cm;
	G4double protbox_y = 500.0*cm;
	G4double protbox_z = 500.0*cm;

	G4Box* protbox = new G4Box("prot_box", protbox_x, protbox_y, protbox_z);

	protbox_log = new G4LogicalVolume(protbox, H2O, "prot_box_log");
	protbox_phys = new G4PVPlacement(0,
	    G4ThreeVector(0., 0., 0.),
		protbox_log,
		"protbox",
		0,
		false,
		0);

	return protbox_phys;
}

void ProtboxDetectorConstruction::InitPhantom(const G4String filename) {
    int num_x, num_y, num_z;
    double x_min, y_min, z_min;
    double x_max, y_max, z_max;
    double half_x, half_y, half_z;
    std::vector<G4Material*> g4_mats;
    G4ThreeVector shift;

    phant.load_from_egsphant(filename);

    for (int i = 0; i < phant.material_names.size(); i++) {
        g4_mats.push_back(G4Material::GetMaterial(phant.material_names[i]));
    }

    x_min = phant.get_xmin() * cm;
    x_max = phant.get_xmax() * cm;
    y_min = phant.get_ymin() * cm;
    y_max = phant.get_ymax() * cm;
    z_min = phant.get_zmin() * cm;
    z_max = phant.get_zmax() * cm;

    half_x = phant.get_x_size() / 2.0 * cm;
    half_y = phant.get_y_size() / 2.0 * cm;
    half_z = phant.get_z_size() / 2.0 * cm;

    num_x = phant.get_num_x();
    num_y = phant.get_num_y();
    num_z = phant.get_num_z();

    shift = G4ThreeVector((x_max + x_min) / 2.0, (y_max + y_min) / 2.0, (z_max + z_min) / 2.0);

    std::cout << shift << G4endl;

    G4Material* water = G4Material::GetMaterial("G4_WATER");

    G4Box* phantomVoxel_vol = new G4Box("phantomVoxel_vol", half_x, half_y, half_z);
    phantomVoxel_log = new G4LogicalVolume(phantomVoxel_vol, water,"phantomVoxel_log", 0, 0, 0);

    cont_vol = new G4Box("cont_vol", num_x * half_x, num_y * half_y, num_z * half_z);
    cont_log = new G4LogicalVolume(cont_vol, water, "cont_log", 0, 0, 0);
    cont_phys = new G4PVPlacement(0, shift, cont_log, "cont_phys", protbox_log, false, 1);

    param = new G4PhantomParameterisation();
    param->SetVoxelDimensions(half_x, half_y, half_z);
    param->SetNoVoxel(num_x, num_y, num_z);
    param->SetMaterials(g4_mats);
    param->SetMaterialIndices(phant.material_ids);
    param->SetSkipEqualMaterials(false);
    param->BuildContainerSolid(cont_phys);
    param->CheckVoxelsFillContainer(cont_vol->GetXHalfLength(),cont_vol->GetYHalfLength(),cont_vol->GetZHalfLength());

    phantom_phys = new G4PVParameterised("phantom_phys", phantomVoxel_log, cont_log, kZAxis, param->GetNoVoxel(), param);
    phantom_phys->SetRegularStructureId(1);

    G4MultiFunctionalDetector* myScorer = new G4MultiFunctionalDetector("myScorer");
    //G4VPrimitiveScorer* totalDose = new MyDoseDeposit("DoseDeposit", num_x, num_y, num_z);
    G4VPrimitiveScorer* totalDose = new G4PSDoseDeposit("DoseDeposit", 0);

    myScorer->RegisterPrimitive(totalDose);

    G4SDManager::GetSDMpointer()->AddNewDetector(myScorer);
    phantomVoxel_log->SetSensitiveDetector(myScorer);
}

void ProtboxDetectorConstruction::SetMaterial(const G4String& mat)
{
  // search the material by its name
  G4Material* material = G4Material::GetMaterial(mat);

  if (material) {
    if(protbox_log) protbox_log->SetMaterial(material);
    G4RunManager::GetRunManager()->PhysicsHasBeenModified();
  }
}

