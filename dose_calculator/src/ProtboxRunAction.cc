//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: ProtboxRunAction.cc 74809 2013-10-22 09:49:26Z gcosmo $
//
/// \file ProtboxRunAction.cc
/// \brief Implementation of the ProtboxRunAction class
//

#include "ProtboxRunAction.hh"
#include "ProtboxRun.hh"

//-- In order to obtain detector information.
#include <fstream>
#include <iomanip>
#include <iostream>
#include "G4THitsMap.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4VUserPrimaryGeneratorAction.hh"
#include "ProtboxDetectorConstruction.hh"
#include "ProtboxPrimaryGeneratorAction.hh"
#include "G4RunManager.hh"
#include "string_utils.hh"


//ProtboxRunAction* ProtboxRunAction::fgInstance = 0;

//ProtboxRunAction* ProtboxRunAction::Instance() { return fgInstance; }

ProtboxRunAction::ProtboxRunAction(G4String fileName): G4UserRunAction() {
    fSDName.push_back(G4String("myScorer"));
    rFilename = fileName;
}

ProtboxRunAction::~ProtboxRunAction() {
    fSDName.clear();
}

G4Run* ProtboxRunAction::GenerateRun() {
    return pbrun = new ProtboxRun(fSDName);
}

void ProtboxRunAction::setInputFilename(G4String input_filename) {
    rFilename = input_filename;
}

void ProtboxRunAction::BeginOfRunAction(const G4Run* aRun) {
    G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;
    //inform the runManager to save random number seed
    G4RunManager::GetRunManager()->SetRandomNumberStore(false);
}

void ProtboxRunAction::EndOfRunAction(const G4Run* aRun) {
    G4int nofEvents = aRun->GetNumberOfEvent();
    if (nofEvents == 0) return;

    const ProtboxRun* reRun = static_cast<const ProtboxRun*>(aRun);
    G4THitsMap<G4double>* DoseDeposit = reRun->GetHitsMap(fSDName[0]+"/DoseDeposit");

    G4cout << "=============================================================" <<G4endl;
    G4cout << " Number of event processed : "<< nofEvents << G4endl;
    G4cout << "=============================================================" <<G4endl;

    Export_3ddose(DoseDeposit, &(reRun->fSqMap), nofEvents);
}

void ProtboxRunAction::Export_3ddose(G4THitsMap<G4double>* DoseDeposit, const G4THitsMap<G4double>* sqDoseDeposit, int n_hist) {
    std::ofstream dose_file;
    G4double uncert_value, inv_hist;
    int num_x, num_y, num_z;
    
    G4String dose_filename = split(std::string(rFilename), '.')[0] + ".3ddose";
    dose_file.open(dose_filename);
    G4cout << "Exporting dose to " << dose_filename << G4endl;

    const ProtboxDetectorConstruction *det = static_cast<const ProtboxDetectorConstruction*>(G4RunManager::GetRunManager()->GetUserDetectorConstruction());
    num_x = det->phant.num_voxels[0];
    num_y = det->phant.num_voxels[1];
    num_z = det->phant.num_voxels[2];

    std::vector<G4double> dose_matrix(num_x * num_y * num_z, 0.0);
    std::vector<G4double> uncert_matrix(num_x * num_y * num_z, 0.0);

    const ProtboxPrimaryGeneratorAction *act = static_cast<const ProtboxPrimaryGeneratorAction*>(G4RunManager::GetRunManager()->GetUserPrimaryGeneratorAction());

    // DOSXYZnrc normalises dose by fluence, and we need the source's area. This way of getting the area is not
    // the best, it assumes that only one source is being used with the GeneralParticleSource class.
    // If more than one source is used, then the normalisation is meaningless anyway.
    G4double fluence = act->getGPS()->GetCurrentSource()->GetPosDist()->GetHalfX() / cm * act->getGPS()->GetCurrentSource()->GetPosDist()->GetHalfY() / cm * 4;

    // Hitmaps are essentially hash tables where the key (itr->first) is the voxel index
    // and the value (itr->second) is the quantity being accumulated by the scorer.
    std::map<G4int,G4double*>::iterator itr = DoseDeposit->GetMap()->begin();
    for(; itr != DoseDeposit->GetMap()->end(); itr++) {
        dose_matrix[itr->first] = (*(itr->second) / CLHEP::gray) * (fluence / n_hist);
    }

    std::map<G4int,G4double*>::iterator it2 = sqDoseDeposit->GetMap()->begin();
    for(; it2 != sqDoseDeposit->GetMap()->end(); it2++) {
        uncert_matrix[it2->first] = (*(it2->second) / (CLHEP::gray * CLHEP::gray)) * (fluence * fluence / n_hist);
    }

    // 3ddose format, all numbers separated by spaces
    // ----------------------------------------------------------------------
    // Number of voxels (num_x, num_y, num_z)
    // x voxel boundaries (num_x + 1 floats)
    // y voxel boundaries (num_y + 1 floats)
    // z voxel boundaries (num_z + 1 floats)
    // linearised dose grid (index = x + y * num_x + z * num_x * num_y)
    // linearised uncertainty grid (index = x + y * num_x + z * num_x * num_y)

    dose_file << det->phant.num_voxels[0] << " " << det->phant.num_voxels[1] << " " << det->phant.num_voxels[2] << G4endl;

    for (int j = 0; j < 3; j++) {
        for (int i = 0; i < det->phant.num_voxels[j] + 1; i++) {
            if (i != 0) dose_file << " ";
            dose_file << det->phant.topleft[j] + i * det->phant.voxel_size[j];
        }
        dose_file << G4endl;
    }

    dose_file << std::scientific;
    for (int i = 0; i < dose_matrix.size(); i++) {
        if (i != 0) dose_file << " ";
        dose_file << dose_matrix[i];
    }
    dose_file << G4endl;

    inv_hist = 1.0 / (n_hist - 1);

    for (int i = 0; i < uncert_matrix.size(); i++) {
        if (i != 0) dose_file << " ";
        if (uncert_matrix[i] > 0.0) {
            uncert_value = sqrt((uncert_matrix[i] - dose_matrix[i] * dose_matrix[i]) * inv_hist) / dose_matrix[i];
        }
        else {
            uncert_value = 0.0;
        }
        dose_file << uncert_value;
    }
    dose_file.close();

}

