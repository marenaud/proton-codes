#include "globals.hh"
#include "Phantom.hh"
#include <fstream>
#include <vector>
#include <iostream>
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>
#include "string_utils.hh"

Phantom::Phantom(){}
Phantom::~Phantom(){}

void Phantom::load_from_egsphant(G4String phant_filename) {
    G4double dummy;
    G4int total_vox;
    std::vector<G4String> mats;
    std::string buffer, buffer2;
    std::stringstream convertor;

    std::ifstream phantom_file(phant_filename);
    p_filename = phant_filename;
    std::getline(phantom_file, buffer);
    convertor.str(buffer);

    convertor >> num_mats;
    std::cout << "Num mats: " << num_mats << std::endl;
    for (int i = 0; i < num_mats; i++) {
        std::getline(phantom_file, buffer);
        std::cout << buffer << std::endl;
        material_names.push_back(trim(buffer));
    }
    std::getline(phantom_file, buffer); // dummy

    std::getline(phantom_file, buffer); // number of voxels
    sscanf(buffer.c_str(), "%i %i %i", &num_voxels[0], &num_voxels[1], &num_voxels[2]);
    std::cout << "Number of voxels: (" << num_voxels[0] << ", " << num_voxels[1] << ", " << num_voxels[2] << ")" << std::endl;
    for (int i = 0; i < 3; i++) {
        std::getline(phantom_file, buffer);
        sscanf(buffer.c_str(), "%lf %lf %*s", &topleft[i], &dummy);
        voxel_size[i] = dummy - topleft[i];
    }
    std::cout << "Voxel size: (" << voxel_size[0] << ", " << voxel_size[1] << ", " << voxel_size[2] << ")" << std::endl;

    total_vox = num_voxels[0] * num_voxels[1] * num_voxels[2];
    material_ids = new size_t[total_vox];
    int big_counter = 0;
    for (int k = 0; k < num_voxels[2]; k++) {
        for (int j = 0; j < num_voxels[1]; j++) {
            std::getline(phantom_file, buffer); // Material values
            const char *buf_dup = buffer.c_str();
            for (int i = 0; i < num_voxels[0]; i++) {
                material_ids[big_counter] = buf_dup[i] - '0' - 1;
                big_counter++;
            }
        }
        std::getline(phantom_file, buffer);
    }

    std::cout << "Phantom Dimensions: (" << topleft[0] << " to " << get_xmax() << "), (" << topleft[1] << " to " << get_ymax() << "), (" << topleft[2] << " to " << get_zmax() << ")" <<  std::endl;

    phantom_file.close();

    // Densities not loaded right now.
}

G4double Phantom::get_xmin() {
    return topleft[0];
}

G4double Phantom::get_ymin() {
    return topleft[1];
}

G4double Phantom::get_zmin() {
    return topleft[2];
}

G4double Phantom::get_xmax() {
    return topleft[0] + num_voxels[0] * voxel_size[0];
}

G4double Phantom::get_ymax() {
    return topleft[1] + num_voxels[1] * voxel_size[1];
}

G4double Phantom::get_zmax() {
    return topleft[2] + num_voxels[2] * voxel_size[2];
}

G4double Phantom::get_x_size() {
    return voxel_size[0];
}

G4double Phantom::get_y_size() {
    return voxel_size[1];
}

G4double Phantom::get_z_size() {
    return voxel_size[2];
}

G4double Phantom::get_num_x() {
    return num_voxels[0];
}

G4double Phantom::get_num_y() {
    return num_voxels[1];
}

G4double Phantom::get_num_z() {
    return num_voxels[2];
}
