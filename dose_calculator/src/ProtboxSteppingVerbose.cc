//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************

#include "ProtboxSteppingVerbose.hh"
#include "G4SteppingManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

ProtboxSteppingVerbose::ProtboxSteppingVerbose()
{}


ProtboxSteppingVerbose::~ProtboxSteppingVerbose()
{}


void ProtboxSteppingVerbose::StepInfo()
{
  CopyState();

  G4int prec = G4cout.precision(6);
  G4ThreeVector pos, mom;

  if( verboseLevel >= 1 ){
    //if( verboseLevel >= 4 ) VerboseTrack();

    if(fStep->GetPostStepPoint()->GetProcessDefinedStep() != NULL){
        G4cout << "   " << std::setw(10) << fStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName();
    } else {
        G4cout << "   UserLimit";
    }

    pos = fTrack->GetPosition();
    mom = fTrack->GetMomentumDirection();

    G4cout << std::setw(15) << pos.x()/cm
	<< std::setw(15) << pos.y()/cm
	<< std::setw(15) << pos.z()/cm
	<< std::setw(15) << mom.x()
    << std::setw(15) << mom.y()
    << std::setw(15) << mom.z()
	<< std::setw(15) << fTrack->GetKineticEnergy()/MeV
	<< std::setw(15) << fStep->GetTotalEnergyDeposit()/MeV
	<< "  ";

    G4cout << G4endl;

    if( verboseLevel >= 1 ){
      G4int tN2ndariesTot = fN2ndariesAtRestDoIt +
	                    fN2ndariesAlongStepDoIt +
	                    fN2ndariesPostStepDoIt;
      if(tN2ndariesTot>0){

        for(size_t lp1=(*fSecondary).size() - tN2ndariesTot; lp1<(*fSecondary).size(); lp1++){
          pos = (*fSecondary)[lp1]->GetPosition();
          mom = (*fSecondary)[lp1]->GetMomentumDirection();

          G4cout << "     Secondary"
             << std::setw(10)
             << (*fSecondary)[lp1]->GetDefinition()->GetParticleName()
             << std::setw(15)
             << pos.x()/cm
             << std::setw(15)
             << pos.y()/cm
             << std::setw(15)
             << pos.z()/cm
             << std::setw(15)
             << mom.x()
             << std::setw(15)
             << mom.y()
             << std::setw(15)
             << mom.z()
             << std::setw(15)
             << (*fSecondary)[lp1]->GetKineticEnergy()/MeV;
             G4cout << G4endl;
        }
      }
    }
  }
  G4cout.precision(prec);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ProtboxSteppingVerbose::TrackingStarted()
{
    G4ThreeVector pos, mom;

    CopyState();
    G4int prec = G4cout.precision(3);
    if( verboseLevel > 0 ){
        pos = fTrack->GetPosition();
        mom = fTrack->GetMomentumDirection();

        G4cout << std::setw(15) << "Process"
            << std::setw(2) << "Step#"
            << std::setw(15) << "X"
            << std::setw(15) << "Y"
            << std::setw(15) << "Z"
            << std::setw(15) << "U"
            << std::setw(15) << "V"
            << std::setw(15) << "W"
            << std::setw(15) << "KineE"
            << std::setw(15) << "dEStep"
            << G4endl;

        G4cout  << "      initStep";
        G4cout << std::setw(15) << pos.x()/cm
            << std::setw(15) << pos.y()/cm
            << std::setw(15) << pos.z()/cm
            << std::setw(15) << mom.x()
            << std::setw(15) << mom.y()
            << std::setw(15) << mom.z()
            << std::setw(15) << fTrack->GetKineticEnergy()/MeV
            << std::setw(15) << fStep->GetTotalEnergyDeposit()/MeV
            << "  "
        << G4endl;

    }
    G4cout.precision(prec);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
