#ifndef Phantom_H
#define Phantom_H 1

#include "globals.hh"
#include <vector>

class Phantom {
  public:
    Phantom();
    ~Phantom();

    G4double get_xmin();
    G4double get_xmax();
    G4double get_ymin();
    G4double get_ymax();
    G4double get_zmin();
    G4double get_zmax();
    G4double get_x_size();
    G4double get_y_size();
    G4double get_z_size();
    G4double get_num_x();
    G4double get_num_y();
    G4double get_num_z();
    G4int num_voxels[3];
    G4double voxel_size[3];
    G4double topleft[3];

    G4String p_filename;

    int num_mats;
    size_t *material_ids;
    std::vector<G4String> material_names;
    void load_from_egsphant(const G4String);
  private:
    // Logical volumes
    //
};

#endif
