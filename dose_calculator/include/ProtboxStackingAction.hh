#ifndef ProtboxStackingAction_H
#define ProtboxStackingAction_H 1

#include "globals.hh"
#include "G4UserStackingAction.hh"
#include "G4ThreeVector.hh"


class G4Track;

class ProtboxStackingAction : public G4UserStackingAction
{
  public:
    ProtboxStackingAction();
    virtual ~ProtboxStackingAction();

  public:
    virtual G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track* aTrack);
};
#endif
