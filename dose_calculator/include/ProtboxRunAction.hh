//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: ProtboxRunAction.hh 74809 2013-10-22 09:49:26Z gcosmo $
//
/// \file medical/DICOM/include/ProtboxRunAction.hh
/// \brief Definition of the ProtboxRunAction class
//

#ifndef ProtboxRunAction_h
#define ProtboxRunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"
#include "G4THitsMap.hh"
#include "Phantom.hh"
#include <vector>

class G4Run;
class ProtboxRun;

class ProtboxRunAction : public G4UserRunAction {
public:
    ProtboxRunAction(G4String);
    virtual ~ProtboxRunAction();

    static ProtboxRunAction* Instance();

public:
    virtual G4Run* GenerateRun();
    virtual void BeginOfRunAction(const G4Run*);
    virtual void EndOfRunAction(const G4Run*);
    void setInputFilename(G4String);
    void Export_3ddose(G4THitsMap<double>*, const G4THitsMap<double>*, int);

    ProtboxRun* GetProtboxRun() const { return pbrun; }

public:
    std::string FillString(const std::string &name, char c, G4int n, G4bool back=true);
    Phantom phant;
private:
    static ProtboxRunAction* fgInstance;
    G4String rFilename;
    ProtboxRun* pbrun;
    std::vector<G4String> fSDName;
};

//

#endif
