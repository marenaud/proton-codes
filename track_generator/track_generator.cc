#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "ProtboxDetectorConstruction.hh"
#include "ProtboxPrimaryGeneratorAction.hh"
#include "ProtboxPhysicsList.hh"
#include "ProtboxStackingAction.hh"
#include "ProtboxSteppingVerbose.hh"

int main(int argc, char **argv) {
  // User Verbose output class
  //
  G4VSteppingVerbose* verbosity = new ProtboxSteppingVerbose;
  G4VSteppingVerbose::SetInstance(verbosity);

  // Construct the default run manager
  //
  G4RunManager* runManager = new G4RunManager;

  // set mandatory initialization classes
  //
  G4VUserDetectorConstruction* detector = new ProtboxDetectorConstruction;
  runManager->SetUserInitialization(detector);

  G4VUserPhysicsList* physics = new ProtboxPhysicsList();
  runManager->SetUserInitialization(physics);

  runManager->Initialize();


  // set mandatory user action class
  //
  G4VUserPrimaryGeneratorAction* gen_action = new ProtboxPrimaryGeneratorAction;
  runManager->SetUserAction(gen_action);

  G4UserStackingAction* stacking_action = new ProtboxStackingAction;
  runManager->SetUserAction(stacking_action);

  // Get the pointer to the UI manager and set verbosities
  //
  G4UImanager* UI = G4UImanager::GetUIpointer();

  if (argc != 1) {
     G4String command = "/control/execute ";
     G4String fileName = argv[1];
     UI->ApplyCommand(command+fileName);
  }

  delete runManager;
  delete verbosity;
  return 0;

}
