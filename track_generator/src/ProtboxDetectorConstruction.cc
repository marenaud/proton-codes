#include "ProtboxDetectorConstruction.hh"
#include "ProtboxDetectorMessenger.hh"
#include "G4NistManager.hh"
#include "G4RunManager.hh"
#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "globals.hh"
#include "G4UserLimits.hh"

#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

ProtboxDetectorConstruction::ProtboxDetectorConstruction()
 :  protbox_log(0), protbox_phys(0)
{
    fMessenger = new ProtboxDetectorMessenger(this);
}

ProtboxDetectorConstruction::~ProtboxDetectorConstruction()
{
	delete stepLimit;
    delete fMessenger;
}

G4VPhysicalVolume* ProtboxDetectorConstruction::Construct()
{
	G4NistManager* man = G4NistManager::Instance();
    G4int nel;

	G4Material* H2O  = man->FindOrBuildMaterial("G4_WATER");
    G4Material* Air  = man->FindOrBuildMaterial("G4_AIR");

    G4Material* bone = man->FindOrBuildMaterial("G4_BONE_CORTICAL_ICRP");

    G4Element* elH  = man->FindOrBuildElement( 1);
    G4Element* elC  = man->FindOrBuildElement( 6);
    G4Element* elN  = man->FindOrBuildElement( 7);
    G4Element* elO  = man->FindOrBuildElement( 8);
    G4Element* elNa = man->FindOrBuildElement(11);
    G4Element* elMg = man->FindOrBuildElement(12);
    G4Element* elP  = man->FindOrBuildElement(15);
    G4Element* elS  = man->FindOrBuildElement(16);
    G4Element* elCl = man->FindOrBuildElement(17);
    G4Element* elAr = man->FindOrBuildElement(18);
    G4Element* elK  = man->FindOrBuildElement(19);
    G4Element* elCa = man->FindOrBuildElement(20);
    G4Element* elI  = man->FindOrBuildElement(53);


    G4Material *lung = new G4Material("LUNG",0.26 * g/cm3,nel=9);
    lung->AddElement(elH, 0.103);
    lung->AddElement(elC, 0.105);
    lung->AddElement(elN, 0.031);
    lung->AddElement(elO, 0.749);
    lung->AddElement(elNa,0.002);
    lung->AddElement(elP, 0.002);
    lung->AddElement(elS, 0.003);
    lung->AddElement(elCl,0.003);
    lung->AddElement(elK, 0.002);

	G4double protbox_x = 100.0*km;
	G4double protbox_y = 100.0*km;
	G4double protbox_z = 100.0*km;

	G4Box* protbox = new G4Box("prot_box", protbox_x, protbox_y, protbox_z);

	protbox_log = new G4LogicalVolume(protbox, H2O, "prot_box_log");
	protbox_phys = new G4PVPlacement(0,
	    G4ThreeVector(0., 0., 0.),
		protbox_log,
		"protbox",
		0,
		false,
		0);

    stepLimit = new G4UserLimits(100.0*km);
	protbox_log->SetUserLimits(stepLimit);


	return protbox_phys;
}

void ProtboxDetectorConstruction::SetMaterial(const G4String& mat)
{
  // search the material by its name
  G4Material* material = G4Material::GetMaterial(mat);

  if (material) {
    if(protbox_log) protbox_log->SetMaterial(material);
    G4RunManager::GetRunManager()->PhysicsHasBeenModified();
  }
}

void ProtboxDetectorConstruction::SetMaxStep(G4double maxStep)
{
  if ((stepLimit)&&(maxStep>0.)) stepLimit->SetMaxAllowedStep(maxStep);
}

