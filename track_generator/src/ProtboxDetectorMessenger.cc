//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************

#include "ProtboxDetectorMessenger.hh"

#include "ProtboxDetectorConstruction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithoutParameter.hh"

ProtboxDetectorMessenger::ProtboxDetectorMessenger(ProtboxDetectorConstruction * Det)
:G4UImessenger(), fDetector(Det)
{
  ftestDir = new G4UIdirectory("/track_generator/");
  ftestDir->SetGuidance(" Track Generator.");

  fmatCmd = new G4UIcmdWithAString("/track_generator/mat",this);
  fmatCmd->SetGuidance("Select Material for tracks");
  fmatCmd->SetParameterName("tMaterial",false);
  fmatCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  fmatCmd->SetToBeBroadcasted(false);
}

ProtboxDetectorMessenger::~ProtboxDetectorMessenger()
{
  delete fmatCmd;
  delete ftestDir;
}

void ProtboxDetectorMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
  if( command == fmatCmd ) {
    fDetector->SetMaterial(newValue);
  }
}

