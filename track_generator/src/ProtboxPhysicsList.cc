#include "ProtboxPhysicsList.hh"

#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

#include "G4RunManager.hh"
#include "G4Region.hh"
#include "G4RegionStore.hh"
#include "G4PhysListFactory.hh"
#include "G4VPhysicsConstructor.hh"

#include "G4StepLimiter.hh"
#include "G4UserSpecialCuts.hh"

#include "G4HadronPhysicsQGSP_BIC.hh"
#include "G4EmStandardPhysics_option3.hh"
#include "G4EmLivermorePhysics.hh"
#include "G4EmPenelopePhysics.hh"
#include "G4EmExtraPhysics.hh"
#include "G4StoppingPhysics.hh"
#include "G4DecayPhysics.hh"
#include "G4HadronElasticPhysics.hh"
#include "G4HadronElasticPhysicsHP.hh"
#include "G4RadioactiveDecayPhysics.hh"
#include "G4IonBinaryCascadePhysics.hh"
#include "G4DecayPhysics.hh"
#include "G4NeutronTrackingCut.hh"
#include "G4LossTableManager.hh"
#include "G4UnitsTable.hh"
#include "G4ProcessManager.hh"
#include "G4IonFluctuations.hh"
#include "G4IonParametrisedLossModel.hh"
#include "G4EmProcessOptions.hh"
#include "G4ParallelWorldPhysics.hh"

ProtboxPhysicsList::ProtboxPhysicsList() : G4VModularPhysicsList()
{
    emPhysicsList = new G4EmStandardPhysics_option3();
    hadronPhys.push_back( new G4HadronPhysicsQGSP_BIC());
    hadronPhys.push_back( new G4EmExtraPhysics());
    hadronPhys.push_back( new G4HadronElasticPhysics());
    hadronPhys.push_back( new G4StoppingPhysics());
    hadronPhys.push_back( new G4IonBinaryCascadePhysics());
    hadronPhys.push_back( new G4NeutronTrackingCut());

    // Decay physics and all particles
    decPhysicsList = new G4DecayPhysics();
    raddecayList = new G4RadioactiveDecayPhysics();

}

void ProtboxPhysicsList::ConstructParticle()
{

  decPhysicsList->ConstructParticle();

}


ProtboxPhysicsList::~ProtboxPhysicsList()
{
  delete emPhysicsList;
  delete decPhysicsList;
  delete raddecayList;
  for(size_t i=0; i<hadronPhys.size(); i++) {delete hadronPhys[i];}
}

void ProtboxPhysicsList::ConstructProcess()
{
  // transportation
  AddTransportation();

  // electromagnetic physics list
  emPhysicsList->ConstructProcess();
  em_config.AddModels();

  decPhysicsList->ConstructProcess();
  raddecayList->ConstructProcess();

  // hadronic physics lists
  for(size_t i=0; i<hadronPhys.size(); i++) {
    hadronPhys[i] -> ConstructProcess();
  }

  // step limitation (as a full process)
  //
  AddStepMax();
}

void ProtboxPhysicsList::AddStepMax()
{
  // Step limitation seen as a process
  G4StepLimiter* stepLimiter = new G4StepLimiter();
  ////G4UserSpecialCuts* userCuts = new G4UserSpecialCuts();

  theParticleIterator->reset();
  while ((*theParticleIterator)()){
      G4ParticleDefinition* particle = theParticleIterator->value();
      G4ProcessManager* pmanager = particle->GetProcessManager();

      if (particle->GetPDGCharge() != 0.0)
        {
          pmanager ->AddDiscreteProcess(stepLimiter);
          ////pmanager ->AddDiscreteProcess(userCuts);
        }
  }
}


void ProtboxPhysicsList::SetCuts()
{
	SetCutsWithDefault();
  SetCutValue(10 * cm, "e-");

}
