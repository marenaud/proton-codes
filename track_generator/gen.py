import sys
import os
from subprocess import call

energies = [20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 240, 250]
medium = sys.argv[1]
ntracks = 20000

for energy in energies:
    filename = "%iMeV_%s" % (energy, medium)
    bank_filename = "/home/marc/tracks/pre-%s" % filename
    with open(filename + ".mac", "w") as inputfile:
        inputfile.write("""/control/verbose 0
/run/verbose 0
/tracking/verbose 2

/run/initialize

/track_generator/mat %s

/gun/particle proton
/gun/energy %i MeV
/gun/direction 0.0 0.0 1.0
/gun/position 0 0 0

/run/beamOn %i
""" % (medium, energy, ntracks))

    call("./track_generator %s > %s" % (filename + ".mac", bank_filename + ".temp"), shell=True)
    call("sed -n '/^* G4Track Information/ { s///; :a; n; p; ba; }' %s > %s" % (bank_filename + ".temp", bank_filename), shell=True)
    os.remove(bank_filename + ".temp")
